#####################################################################
Jasmine Paints Color Visualizer
#####################################################################





#####################################################################
User Guide
#####################################################################








Jasmine Paints Pvt. Ltd., Chitwan, Nepal
Phone : +977 56-532789



===========================
Legal Notices
============================

CAUTION:
This documentation is intended for users who will be using the Color Visualizer Software Branded as “Jasmine Paints Color Visualizer”. The “Jasmine Paints Color Visualizer” Software is inherently simple, and the sample images in application and usage guide  in this documentation are not intended to be exhaustive or to apply to any particular situation. Users are cautioned to satisfy themselves as to the accuracy and results of colouring walls and shades.

“Jasmine Paints Color Visualizer” Software shall not be responsible for the accuracy of colour using the “Jasmine Paints Color Visualizer” Software or the procedures, examples, or explanations in this documentation. “Jasmine Paints Color Visualizer” Software shall not
be responsible for the consequences of any errors or omissions that may appear in this documentation.

“Jasmine Paints Color Visualizer” Software is available only under license from Jasmine Paints Pvt. Ltd., Chitwan, Nepal. 

This documentation is subject to the terms and conditions. This documentation and the software described in this documentation are subject to change without prior notice.

No part of this documentation may be reproduced or distributed in any form without prior written permission of Jasmine Paints Pvt. Ltd., Chitwan, Nepal.




Jasmine Paints Pvt. Ltd., Chitwan, Nepal
Phone : +977 56-532789

©Jasmine Paints Pvt. Ltd., 2017


For additional information concerning trademarks, copyrights, and licenses, see the Legal Notices in the “Jasmine Paints Color Visualizer”  1.1.1.0 user guide.


=================
Guide Overview 
=================

Description of this guide

This Color Visualizer Application Branded as “Jasmine Paints Color Visualizer” is to visualize the color spectra on walls of buildings and housings. This gives the initial but perfect visual effect to the walls with combination and options of millions of color. User can draw multiple shapes according to their need and requirements as well as spread color to the drawn shades with correction in brightness and contrast of the shade. They can use Sample Images as reference for quick colour visualization purpose.
This User Guide describes the steps to visualize the color spectra on walls of buildings and housings. It provides the visualization of different colours to the walls necessary for a customer and service provider for analysis as well as to import colour data from a data source, and use that data to create and modify colours on walls.

This guide is organized into the following sections:

**Chapter 1**      
Getting Started with “Jasmine Paints Color Visualizer”

introduces “Jasmine Paints Color Visualizer” Software. It describes about color matching tips,  Check points before installation, Tips on getting a good match, Factors affecting a good match with the Colour Spectra, how to install  “Jasmine Paints Color Visualizer” and how it works.

**Chapter 2**      
Introduction to different sections in application describes the sections and components they contain.

**Chapter 3**         
Sample Images describes using sample images.

**Chapter 4**          
My Images describes using my images.

**Chapter 5**                
Saved Images describes using saved images.

**Chapter 6**            
Tools describes about different tools and their usage.

**Chapter 7**                    
Menu section describes about menu items and their usage.

**Chapther 8**                  
This section describes about all other remaining section and techniques in application

**Chapter 9**                   
Shortcut section provides keyboard shortcuts to use in software.


==========================================================
Getting Started With “Jasmine Paints Color Visualizer”        
==========================================================

Colour Matching Tips     
===========================
Colour representation in the “Jasmine Paints Color Visualizer” Software

This application gives the true representation of shades in a room, given the light conditions at the time of clicking the photograph. If the image has dark shadows, then the colour too will darken over the shadows. When applied on the walls of your house, the chosen shade should look similar to what it looks in the visualizer, provided the light conditions of your room remain similar. To lessen the effect of shadows use the "shade corrector" tool.             

Check points before installing the “Jasmine Paints Color Visualizer” Software
===================================================================================
1) Natural or white light is the correct light to compare Colour Spectra shade with the shades displayed on screen. Please ensure there is bright white light around the computer. The light should fall directly on the Colour Spectra shade tab. Please hold the shade tab away from the computer screen.       
• Please do not let light from the computer screen fall on the Colour Spectra shade tab, while comparing.         
2) Please ensure your PC is running on the default display profile of windows .       
 • Right click on the desktop window> Click on properties-> Click on settings-, Click on advanced.-> Click on Colour management. Remove all available display profiles. The windows default profile will take over.

Tips on getting a good match   
==========================================
1) If the shades on the screen look lighter or darker than those on the Colour Spectra, please adjust the brightness settings of your screen.       

Factors affecting a good match with the Colour Spectra    
===========================================================
1) Shade matches may get affected by the monitor as per the following:        
Settings of the monitor - Please ensure the computer is running on the windows default display profile.           
• Age of the monitor - All or some specific shade families may show variation.          
• Brand of the monitor.      
• Type of monitor - CRT monitors can display more colours as compared to LCD monitors.      
• Quality of monitor - Some monitors are specifically designed for good colour representation.           

The “Jasmine Paints Color Visualizer” Software is a tool to help the consumer imagine colour combinations on his walls. It is not a tool for selecting shades. We strongly recommend that the final shade verification be done from the "Colour Spectra", before purchase. 

System Requirements      
========================
• Operating Systems -Windows 7, 8, 8.1, 10.         
• RAM -1 GB & Above          
• Free hard disk space -250MB              


Software installation       
===========================
Find the Color Visualizer application name ColorVisualizerVer**.exe file to install software as in image below.      

.. image:: ../../images/applocation.jpg          

Double Click on application to install. Following screen will appear.             

.. image:: ../../images/appinstallation.jpg             

After few time application will be completely installed on your computer and ready to use. 
You can see application shortcut on your desktop. Double click on it, the application will now open.


===================
Working Frame      
===================

This is the main working window as shown in image below.          

.. image:: ../../images/HomeScreen1.jpg          

    1. Menu Section            
    2. Tools Section               
    3. Image Section             
    4. Recent Shade Section            
    5. Shades Used Section              
    6. Sarch Button                 
    7. Selected Color Details             
    8. Color Family Picker           
    9. Color Picker                 


Sample Images      
================    

.. image:: ../../images/sampleimage.jpg      

There are few sample images in the different categories to chose and start painting. These samples are default images and can be colored as per requirement.
It Contain two tabs for interior and exterior sample images.         
        a. Image workshop name shown at navigation list.       
        b. Project images of respective workshop are shown at detailed pane.        


My Images         
============   

.. image:: ../../images/myimage.jpg          


Saved Images        
================

.. image:: ../../images/savedimage.jpg      

Working project can be saved as file so that you can continue working saving as file. Any working project can be opened again and start working for painting project.             


Menu                 
========

.. image:: ../../images/TopMenuBar.jpg     
        
            a. File Menu           
                ==============   
                File Menu Contains Four subMenus.                
                    a. Save                 
                        ■ Automatically save changes made on previously saved project.            
                    b. Save As              
                        ■ Saves new project.                
                        ■ Category: Contains category of the image i.e. interior and exterior.          
                        ■ Workshop: Workshop name of the image e.g. My House, Mother's House etc.          
                        ■ Title: Title of the image e.g. Bedroom, Kitchen etc.           
                        ■ Save: Saves image.          
                        ■ Cancel: Cancels the saving process.             
                    c. Print Preview Menu:           
                        ■ Shows print preview window where image can be saved, printed, zoomed in, zoomed out and other page alignment options.

            b. Group Menu           
                =============
                    a. Selected shades are assigned into one group.
                    b. At least two shades must be selected to form a group.

            c. UnGroup Menu
                ===============
                    a. Removes selected shade from a group.
            
            d. Done Menu
                ===============
                    a. Filled Polygon is created from the selected area..
                    b. By default it is disabled.
                    c. It is enabled only after polygon area is selected.

            e. Reset Menu
                =============
                    a. Resets the selected polygon area.
                    b. By default it is disabled.
                    c. It is enabled only after polygon area is selected.

            f. Undo Menu
                   =================
                    a. Restore changes to previous state.
                    b. Disabled, if previous change state is not available.

            g. Redo Menu
                ==============
                    a. Restore changes back to next state.
                    b. Disabled, if next change state is not available.


            Delete Shade Menu
               =================
                    a. Deletes the selected shades. 
                    b. Multiple shades can be deleted at same time.         

            Search Menu
                ===========
                    a. Search color on the basis of shade name or shade code as per selection.

            Help Menu
               ==============
                    a. Contains the help catalog.

            Back Menu
               ==========
                    a. Navigate back to home menu.

Left pane (Tools pane)      
==========================

.. image:: ../../images/Tools.jpg

        Tools
           ============

            a. Brush
            ----------
                    a. Used to draw shades in smaller areas.
                    b. Press left mouse button and drag the mouse to draw.
            
            b. Polygon
            ------------
                    a. Used to draw irregular larger shades.
                    b. Click on left mouse button to draw.

            c. Zoom in
            ------------
                    a. Used to zoom in the image.

            d. Zoom out
            -------------
                    a. Used to zoom out the image.

            e. Fit On Screen
            ------------------
                    a. Fits image on the panel.
                    b. Basically used for large images.

            f. Negative
            --------------
                    a. Used to remove a part of a shade.


Right pane
===============

Contains the color picker.


Center pane
===============

Area where shades are drawn.

Bottom Pane
==================

Contains color search box and used shade details.




========================
Keyboard Shortcuts
========================

    • Ctrl + Left Mouse Button : Select one or more shades           
    • Shift + Left Mouse Button : Select a set of shades that belongs to a group.           
    • Esc Button : While using Polygon Tool, it discards the shape currently being drawn.             
    • Delete Button : While using Polygon Tool, it deletes the last drawn point of a shape one at a time. It acts more or less like undo the point selection.              
    • B Button : Selects the Brush Tool.           
    • P Button : Selects the Polygon Tool.               
    • F Button : Resizes the image back to fit on the screen.                  
    • Ctrl + S Button : Saves the image.                


====================
Contact Info
====================

Jasmine Paints Pvt. Ltd. 
Chitwan, Nepal
+977 56-532789





In case of issues please contact to +977-56-526914, Jasmine Paints Pvt. Ltd. Chitwan, Nepal